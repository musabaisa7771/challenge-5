const express = require('express')
const app = express()
const port = 3000

app.set('view engine','ejs');

app.use(express.static('public'))

app.get('/', (req, res) => {
  res.render('Home');
});

app.get('/Game', (req, res) => {
  res.render('Game');
});

app.get('/Login', (req, res) => {
  res.render('Login');
});

app.get('/Register', (req, res) => {
  res.render('Register');
});

app.use('/',(req, res) => {
  res.status(404);
  res.send('<h1>404<h1>')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

